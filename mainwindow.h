#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QMainWindow>
#include <QtSerialPort/QSerialPort>
#include "serie.h"


namespace Ui {
class MainWindow;
}


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    /*!
     * \brief on_ButtonSerial_clicked : Slot  parametrage de la connexion série
     */
    void on_ButtonSerial_clicked();

    /*!
     * \brief SerialConnect permet  la connexion série
     */
    void SerialConnect(QString PortName);

    /*!
     * \brief on_ButtonDeconexion_clicked Slot se deconnecter
     */
    void on_ButtonDeconexion_clicked();

    /*!
     * \brief on_ButtonLaser_clicked : slot allumer ou d'éteindre le laser
     */
    void on_ButtonLaser_clicked();

    /*!
     * \brief on_ButtonUp_clicked : Slot déplacement  vers le haut
     */
    void on_ButtonUp_clicked();

    /*!
     * \brief on_ButtonDown_clicked : Slot  déplacement  vers le bas
     */
    void on_ButtonDown_clicked();

    /*!
     * \brief on_ButtonLeft_clicked: Slot  déplacement  la gauche
     */
    void on_ButtonLeft_clicked();

    /*!
     * \brief on_ButtonRight_clicked : Slot  déplacement  vers la droite
     */
    void on_ButtonRight_clicked();

    /*!
     * \brief on_ButtonCircle_clicked : Slot  déssiner un cercle 
     */
    void on_ButtonCircle_clicked();

   

    /*!
     * \brief on_ButtonCenter_clicked : Slot  le pointeur laser à la position intiale
     */
    void on_ButtonCenter_clicked();

    
    void DataWritten(qint64);

    void on_ButtonTriangle_clicked();

    void on_ButtonSpirale_clicked();

    void on_ButtonLaserAllumer_clicked();

    void on_pushButton_clicked();
    
private:
    Ui::MainWindow *ui;
    QSerialPort *serial;
    ConnexionSerie *SerialWindow;
    Protocole *ProtocoleBeagleBone;
};

#endif // MAINWINDOW_H



