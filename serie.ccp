#include "serie.h"
#include "ui_serie.h"

ConnexionSerie::Serie(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Serie)
{
    ui->setupUi(this);
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();

    for(int i = 0; i < list.size(); i++)
        ui->ComboBoxLine->addItem(list.at(i).portName());
}

ConnexionSerie::~Serie()
{
    delete ui;
}

void Serie::on_buttonBox_accepted()
{
    emit InitSerialConnection(ui->ComboBoxLine->currentText());
}

void Serie::on_ButtonRefresh_clicked()
{
    ui->ComboBoxLine->clear();
    QList<QSerialPortInfo> list;
    list = QSerialPortInfo::availablePorts();

    for(int i = 0; i < list.size(); i++)
        ui->ComboBoxLine->addItem(list.at(i).portName());
}
