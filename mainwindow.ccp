#include <QApplication>
#include <QtWidgets>
#include "mainwindow.h"
#include <QApplication>
#include <QPushButton>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>
#include <QApplication>
#include <QtWidgets>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    SerialWindow = new ConnexionSerie(this);
    ProtocoleBeagleBone = new Protocole;
    QObject::connect(SerialWindow, SIGNAL(InitSerialConnection(QString)), this, SLOT(SerialConnect(QString)));
}

MainWindow::~MainWindow()
{
    delete SerialWindow;
    delete ProtocoleBeagleBone;
    delete ui;
}

void MainWindow::on_ButtonSerial_clicked()
{
    SerialWindow->show();
}

void MainWindow::SerialConnect(QString PortName)
{
    serial = new QSerialPort(this);
    serial->setPortName(PortName);
    if (serial->open(QIODevice::ReadWrite))
    {
        serial->setBaudRate(QSerialPort::Baud115200);
        serial->setDataBits(QSerialPort::Data8);
        serial->setParity(QSerialPort::NoParity);
        serial->setStopBits(QSerialPort::OneStop);
        serial->setFlowControl(QSerialPort::NoFlowControl);
    }
 void MainWindow::on_ButtonLaser_clicked()
{
    ProtocoleBeagleBone->EteindreLaser();
    serial->write(ProtocoleBeagleBone->GetTrame());
}



void MainWindow::on_ButtonCircle_clicked()
{
    ProtocoleBeagleBone->LancerForme(66);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_Buttonhaut_clicked()
{
    ProtocoleBeagleBone->DeplacerMoteur(-1,0);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_Buttonbas_clicked()
{
    ProtocoleBeagleBone->DeplacerMoteur(1,0);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_Buttongauche_clicked()
{
    ProtocoleBeagleBone->DeplacerMoteur(0,1);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_Buttondroit_clicked()
{
    ProtocoleBeagleBone->DeplacerMoteur(0,-1);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_ButtonCenter_clicked()
{
    ProtocoleBeagleBone->GoToAngle(0,0);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_ButtonTriangle_clicked()
{
    ProtocoleBeagleBone->LancerForme(67);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_ButtonSpirale_clicked()
{
    ProtocoleBeagleBone->LancerForme(68);
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_ButtonLaserAllumer_clicked()
{
    ProtocoleBeagleBone->AllumerLaser();
    serial->write(ProtocoleBeagleBone->GetTrame());
}

void MainWindow::on_pushButton_clicked()
{
    
}   
        