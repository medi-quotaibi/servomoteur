#ifndef SERIE_H
#define SERIE_H
#include <QDialog>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QList>


namespace Ui {
class Serie;
}

class Serie : public QDialog
{
    Q_OBJECT

public:
    explicit Serie(QWidget *parent = 0);
    ~Serie();

signals:
    /*!
     * \brief InitSerialConnection
     *  signal envoyé lors de l'appui du bouton "ok"
     * \param line : Port sur lequel on désire établir la connexion série
     */
    void InitSerialConnection(QString line);

private slots:
    /*!
     * \brief on_buttonBox_accepted
     * Permet d'envoyer le signal "InitSerialConnection".
     */

    void on_buttonBox_accepted();

    /*!
     * \brief on_ButtonRefresh_clicked
     * permet d'actualiser les ports série .
     */
    void on_ButtonRefresh_clicked();

private:
    Ui::Serie *ui;
};

#endif // SERIE_H